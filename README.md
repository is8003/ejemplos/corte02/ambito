# Tipos de almacenamiento de un identificador [^1] 

[^1]: P. Deitel y H. Deitel, C How to Program, Global Edition. Harlow,
England: Pearson Education Limited, 2016

## _Storage Classes_

### Determinan:

- **Duración**: periodo en el que un _identificador_ existe en memoria.
- **Ámbito**: donde un _identificador_ puede ser referido en el
  programa.
- **Enlace**: si un _identificador_ pude ser referido en varios archivos
  fuente o solo en uno.

## _Storage Classes_

### Especificadores:

- `auto`: _"duración automática"_ comportamiento usual en identificadores de
  variables (_“variables automáticas”_).
- `static`: _"duración estática con enlace interno"_[^2], identificador existe durante todo el tiempo de ejecución del
  programa.
- `extern`: _"duración estática con enlace externo"_, comportamiento usual de
  los _identificadores_ de funciones.
- `register`: considerado **obsoleto**, ignorado por la mayoría de
  compiladores.
- `thread_local`: agregado en estándar `C11`, usado en _hilos_ (programación
  avanzada).

[^2]: ¡OJO!: no confundir con ámbito.

# Reglas de Ámbito [^1]

## _Scope Rules_ 

### Ámbito de un identificador:

Es la porción de un programa en el cual el identificador puede ser
referido.

## _Scope Rules_ 

### Tipos de ámbito:

- **Función**: etiquetas (como las usadas en `switch` y `goto`) son los únicos
  identificadores con este tipo de ámbito.
- **Archivo**: identificador declarado por fuera de cualquier función.
- **Bloque**: identificador declarado entre llaves `{}`.
- **Prototipo de función**: aplica únicamente lista de _parámetros_ en
  prototipo.

# Referencias 
