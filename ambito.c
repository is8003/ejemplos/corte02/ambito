#include <stdio.h>

// Prototipo de funciones
void usoLocal(void);
void usoStaticLocal(void);
void usoGlobal(void);

int x = 1; // Variable global

int main(void)
{
	int x = 5; // Variable local a función main

	// Inicio prueba bloque interno
	puts("### Prueba ámbito de bloque ###");
	puts("");

	printf("x local en bloque externo de función main es %d\n", x);

	{ // inicia bloque
		int x = 7; // Variable local en bloque interno

		printf("x local en bloque interno de función main es %d\n", x);     
	} // finaliza bloque

	printf("x local en bloque externo de función main es %d\n", x);

	// Inicio prueba invocación de funciones
	puts("");
	puts("### Prueba invocación funciones - Parte 1 ###");

	// Primera invocación
	usoLocal(); // usoLocal tiene x local automática
	usoStaticLocal(); // usoStaticLocal tiene x local estática
	usoGlobal(); // usoGlobal usa x global

	puts("");
	puts("### Prueba invocación funciones - Parte 2 ###");

	// Segunda invocación
	usoLocal(); // usoLocal re-inicializa x local automática
	usoStaticLocal(); // x local estática mantiene valor previo
	usoGlobal(); // x global también mantiene valor

	puts("");
	puts("### Prueba final función main ###");

	printf("\nx local en función main es %d\n", x);
} 

// usoLocal re-inicializa variable local x durante cada invocación
void usoLocal(void)
{
	int x = 25; // inicializado cada vez que función usoLocal es invocada.

	printf("\nx local en función usoLocal es %d después de entrar a usoLocal\n", x);
	++x;
	printf("x local en función usoLocal es %d antes de salir de usoLocal\n", x);
} 

// usoStaticLocal inicializa variable estática local x solo la primera vez que
// la función es invocada; el valor de x se conserva entre invocaciones de la
// función
void usoStaticLocal(void) {
	static int x = 50; // inicalizado solo una vez.

	printf("\nx local estática es %d entrnado a usoStaticLocal\n", x);
	++x;
	printf("x local estática es %d saliendo de usoStaticLocal\n", x);
}

// función usoGlobal modifica variable global x durante cada invocación
void usoGlobal(void)
{
	printf("\nx global es %d entrando a usoGlobal\n", x);
	x *= 10;
	printf("x global es %d saliendo de usoGlobal\n", x);
}
